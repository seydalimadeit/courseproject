/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { InvoiceDTO } from './invoice.dto';

/**
 * A Contribution DTO object.
 */
export class ContributionDTO extends BaseDTO {
    @ApiModelProperty({ description: 'name field', required: false })
    name: string;

    @ApiModelProperty({ description: 'score field', required: false })
    score: number;

    @ApiModelProperty({ description: 'time field', required: false })
    time: string;

    @ApiModelProperty({ description: 'currencyUnit field', required: false })
    currencyUnit: string;

    @ApiModelProperty({ type: InvoiceDTO, description: 'invoice relationship' })
    invoice: InvoiceDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}

/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { InvoiceDTO } from './invoice.dto';

/**
 * A Transaction DTO object.
 */
export class TransactionDTO extends BaseDTO {
    @ApiModelProperty({ description: 'currencyUnit field', required: false })
    currencyUnit: string;

    @ApiModelProperty({ description: 'date field', required: false })
    date: string;

    @ApiModelProperty({ type: InvoiceDTO, description: 'invoice relationship' })
    invoice: InvoiceDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}

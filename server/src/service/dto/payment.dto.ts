/* eslint-disable @typescript-eslint/no-unused-vars */
import { ApiModelProperty } from '@nestjs/swagger';
import { BaseDTO } from './base.dto';

import { InvoiceDTO } from './invoice.dto';

/**
 * A Payment DTO object.
 */
export class PaymentDTO extends BaseDTO {
    @ApiModelProperty({ description: 'name field', required: false })
    name: string;

    @ApiModelProperty({ description: 'bankAccountNumber field', required: false })
    bankAccountNumber: number;

    @ApiModelProperty({ description: 'scoreNumber field', required: false })
    scoreNumber: number;

    @ApiModelProperty({ type: InvoiceDTO, description: 'invoice relationship' })
    invoice: InvoiceDTO;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}

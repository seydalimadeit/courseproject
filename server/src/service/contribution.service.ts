import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, FindOneOptions } from 'typeorm';
import { ContributionDTO } from '../service/dto/contribution.dto';
import { ContributionMapper } from '../service/mapper/contribution.mapper';
import { ContributionRepository } from '../repository/contribution.repository';

const relationshipNames = [];
relationshipNames.push('invoice');

@Injectable()
export class ContributionService {
    logger = new Logger('ContributionService');

    constructor(@InjectRepository(ContributionRepository) private contributionRepository: ContributionRepository) {}

    async findById(id: string): Promise<ContributionDTO | undefined> {
        const options = { relations: relationshipNames };
        const result = await this.contributionRepository.findOne(id, options);
        return ContributionMapper.fromEntityToDTO(result);
    }

    async findByfields(options: FindOneOptions<ContributionDTO>): Promise<ContributionDTO | undefined> {
        const result = await this.contributionRepository.findOne(options);
        return ContributionMapper.fromEntityToDTO(result);
    }

    async findAndCount(options: FindManyOptions<ContributionDTO>): Promise<[ContributionDTO[], number]> {
        options.relations = relationshipNames;
        const resultList = await this.contributionRepository.findAndCount(options);
        const contributionDTO: ContributionDTO[] = [];
        if (resultList && resultList[0]) {
            resultList[0].forEach((contribution) =>
                contributionDTO.push(ContributionMapper.fromEntityToDTO(contribution)),
            );
            resultList[0] = contributionDTO;
        }
        return resultList;
    }

    async save(contributionDTO: ContributionDTO): Promise<ContributionDTO | undefined> {
        const entity = ContributionMapper.fromDTOtoEntity(contributionDTO);
        const result = await this.contributionRepository.save(entity);
        return ContributionMapper.fromEntityToDTO(result);
    }

    async update(contributionDTO: ContributionDTO): Promise<ContributionDTO | undefined> {
        const entity = ContributionMapper.fromDTOtoEntity(contributionDTO);
        const result = await this.contributionRepository.save(entity);
        return ContributionMapper.fromEntityToDTO(result);
    }

    async deleteById(id: string): Promise<void | undefined> {
        await this.contributionRepository.delete(id);
        const entityFind = await this.findById(id);
        if (entityFind) {
            throw new HttpException('Error, entity not deleted!', HttpStatus.NOT_FOUND);
        }
        return;
    }
}

import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { CreditDTO } from '../../service/dto/credit.dto';
import { CreditService } from '../../service/credit.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/credits')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('credits')
export class CreditController {
    logger = new Logger('CreditController');

    constructor(private readonly creditService: CreditService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: CreditDTO,
    })
    async getAll(@Req() req: Request): Promise<CreditDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.creditService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: CreditDTO,
    })
    async getOne(@Param('id') id: string): Promise<CreditDTO> {
        return await this.creditService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create credit' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: CreditDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() creditDTO: CreditDTO): Promise<CreditDTO> {
        const created = await this.creditService.save(creditDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Credit', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update credit' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CreditDTO,
    })
    async put(@Req() req: Request, @Body() creditDTO: CreditDTO): Promise<CreditDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Credit', creditDTO.id);
        return await this.creditService.update(creditDTO);
    }

    @Put('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update credit with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: CreditDTO,
    })
    async putId(@Req() req: Request, @Body() creditDTO: CreditDTO): Promise<CreditDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Credit', creditDTO.id);
        return await this.creditService.update(creditDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete credit' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Credit', id);
        return await this.creditService.deleteById(id);
    }
}

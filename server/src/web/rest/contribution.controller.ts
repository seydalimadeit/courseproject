import {
    Body,
    Controller,
    Delete,
    Get,
    Logger,
    Param,
    Post as PostMethod,
    Put,
    UseGuards,
    Req,
    UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiUseTags, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { Request } from 'express';
import { ContributionDTO } from '../../service/dto/contribution.dto';
import { ContributionService } from '../../service/contribution.service';
import { PageRequest, Page } from '../../domain/base/pagination.entity';
import { AuthGuard, Roles, RolesGuard, RoleType } from '../../security';
import { HeaderUtil } from '../../client/header-util';
import { LoggingInterceptor } from '../../client/interceptors/logging.interceptor';

@Controller('api/contributions')
@UseGuards(AuthGuard, RolesGuard)
@UseInterceptors(LoggingInterceptor)
@ApiBearerAuth()
@ApiUseTags('contributions')
export class ContributionController {
    logger = new Logger('ContributionController');

    constructor(private readonly contributionService: ContributionService) {}

    @Get('/')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'List all records',
        type: ContributionDTO,
    })
    async getAll(@Req() req: Request): Promise<ContributionDTO[]> {
        const pageRequest: PageRequest = new PageRequest(req.query.page, req.query.size, req.query.sort);
        const [results, count] = await this.contributionService.findAndCount({
            skip: +pageRequest.page * pageRequest.size,
            take: +pageRequest.size,
            order: pageRequest.sort.asOrder(),
        });
        HeaderUtil.addPaginationHeaders(req.res, new Page(results, count, pageRequest));
        return results;
    }

    @Get('/:id')
    @Roles(RoleType.USER)
    @ApiResponse({
        status: 200,
        description: 'The found record',
        type: ContributionDTO,
    })
    async getOne(@Param('id') id: string): Promise<ContributionDTO> {
        return await this.contributionService.findById(id);
    }

    @PostMethod('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Create contribution' })
    @ApiResponse({
        status: 201,
        description: 'The record has been successfully created.',
        type: ContributionDTO,
    })
    @ApiResponse({ status: 403, description: 'Forbidden.' })
    async post(@Req() req: Request, @Body() contributionDTO: ContributionDTO): Promise<ContributionDTO> {
        const created = await this.contributionService.save(contributionDTO);
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Contribution', created.id);
        return created;
    }

    @Put('/')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update contribution' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: ContributionDTO,
    })
    async put(@Req() req: Request, @Body() contributionDTO: ContributionDTO): Promise<ContributionDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Contribution', contributionDTO.id);
        return await this.contributionService.update(contributionDTO);
    }

    @Put('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Update contribution with id' })
    @ApiResponse({
        status: 200,
        description: 'The record has been successfully updated.',
        type: ContributionDTO,
    })
    async putId(@Req() req: Request, @Body() contributionDTO: ContributionDTO): Promise<ContributionDTO> {
        HeaderUtil.addEntityCreatedHeaders(req.res, 'Contribution', contributionDTO.id);
        return await this.contributionService.update(contributionDTO);
    }

    @Delete('/:id')
    @Roles(RoleType.ADMIN)
    @ApiOperation({ title: 'Delete contribution' })
    @ApiResponse({
        status: 204,
        description: 'The record has been successfully deleted.',
    })
    async deleteById(@Req() req: Request, @Param('id') id: string): Promise<void> {
        HeaderUtil.addEntityDeletedHeaders(req.res, 'Contribution', id);
        return await this.contributionService.deleteById(id);
    }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreditController } from '../web/rest/credit.controller';
import { CreditRepository } from '../repository/credit.repository';
import { CreditService } from '../service/credit.service';

@Module({
    imports: [TypeOrmModule.forFeature([CreditRepository])],
    controllers: [CreditController],
    providers: [CreditService],
    exports: [CreditService],
})
export class CreditModule {}

/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { User } from './user.entity';

/**
 * A Invoice.
 */
@Entity('invoice')
export class Invoice extends BaseEntity {
    @Column({ name: 'score', nullable: true })
    score: string;

    @Column({ name: 'currency_unit', nullable: true })
    currencyUnit: string;

    @ManyToOne((type) => User)
    user: User;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}

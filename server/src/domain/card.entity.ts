/* eslint-disable @typescript-eslint/no-unused-vars */
import { Entity, Column, JoinColumn, OneToOne, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { BaseEntity } from './base/base.entity';

import { Invoice } from './invoice.entity';

/**
 * A Card.
 */
@Entity('card')
export class Card extends BaseEntity {
    @Column({ type: 'integer', name: 'score', nullable: true })
    score: number;

    @Column({ type: 'integer', name: 'number', nullable: true })
    number: number;

    @Column({ type: 'integer', name: 'cvv', nullable: true })
    cvv: number;

    @Column({ name: 'date', nullable: true })
    date: string;

    @ManyToOne((type) => Invoice)
    invoice: Invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
}

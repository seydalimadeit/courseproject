import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { CreditDTO } from '../src/service/dto/credit.dto';
import { CreditService } from '../src/service/credit.service';

describe('Credit Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(CreditService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all credits ', async () => {
        const getEntities: CreditDTO[] = (await request(app.getHttpServer()).get('/api/credits').expect(200)).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET credits by id', async () => {
        const getEntity: CreditDTO = (
            await request(app.getHttpServer())
                .get('/api/credits/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create credits', async () => {
        const createdEntity: CreditDTO = (
            await request(app.getHttpServer()).post('/api/credits').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update credits', async () => {
        const updatedEntity: CreditDTO = (
            await request(app.getHttpServer()).put('/api/credits').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update credits from id', async () => {
        const updatedEntity: CreditDTO = (
            await request(app.getHttpServer())
                .put('/api/credits/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE credits', async () => {
        const deletedEntity: CreditDTO = (
            await request(app.getHttpServer())
                .delete('/api/credits/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});

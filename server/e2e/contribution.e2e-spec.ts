import { Test, TestingModule } from '@nestjs/testing';
import request = require('supertest');
import { AppModule } from '../src/app.module';
import { INestApplication } from '@nestjs/common';
import { AuthGuard } from '../src/security/guards/auth.guard';
import { RolesGuard } from '../src/security/guards/roles.guard';
import { ContributionDTO } from '../src/service/dto/contribution.dto';
import { ContributionService } from '../src/service/contribution.service';

describe('Contribution Controller', () => {
    let app: INestApplication;

    const authGuardMock = { canActivate: (): any => true };
    const rolesGuardMock = { canActivate: (): any => true };
    const entityMock: any = {
        id: 'entityId',
    };

    const serviceMock = {
        findById: (): any => entityMock,
        findAndCount: (): any => [entityMock, 0],
        save: (): any => entityMock,
        update: (): any => entityMock,
        deleteById: (): any => entityMock,
    };

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        })
            .overrideGuard(AuthGuard)
            .useValue(authGuardMock)
            .overrideGuard(RolesGuard)
            .useValue(rolesGuardMock)
            .overrideProvider(ContributionService)
            .useValue(serviceMock)
            .compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/GET all contributions ', async () => {
        const getEntities: ContributionDTO[] = (
            await request(app.getHttpServer()).get('/api/contributions').expect(200)
        ).body;

        expect(getEntities).toEqual(entityMock);
    });

    it('/GET contributions by id', async () => {
        const getEntity: ContributionDTO = (
            await request(app.getHttpServer())
                .get('/api/contributions/' + entityMock.id)
                .expect(200)
        ).body;

        expect(getEntity).toEqual(entityMock);
    });

    it('/POST create contributions', async () => {
        const createdEntity: ContributionDTO = (
            await request(app.getHttpServer()).post('/api/contributions').send(entityMock).expect(201)
        ).body;

        expect(createdEntity).toEqual(entityMock);
    });

    it('/PUT update contributions', async () => {
        const updatedEntity: ContributionDTO = (
            await request(app.getHttpServer()).put('/api/contributions').send(entityMock).expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/PUT update contributions from id', async () => {
        const updatedEntity: ContributionDTO = (
            await request(app.getHttpServer())
                .put('/api/contributions/' + entityMock.id)
                .send(entityMock)
                .expect(201)
        ).body;

        expect(updatedEntity).toEqual(entityMock);
    });

    it('/DELETE contributions', async () => {
        const deletedEntity: ContributionDTO = (
            await request(app.getHttpServer())
                .delete('/api/contributions/' + entityMock.id)
                .expect(204)
        ).body;

        expect(deletedEntity).toEqual({});
    });

    afterEach(async () => {
        await app.close();
    });
});

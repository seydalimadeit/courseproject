import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class ContributionUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.contribution.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#contribution-name'));

  scoreInput: ElementFinder = element(by.css('input#contribution-score'));

  timeInput: ElementFinder = element(by.css('input#contribution-time'));

  currencyUnitInput: ElementFinder = element(by.css('input#contribution-currencyUnit'));

  invoiceSelect = element(by.css('select#contribution-invoice'));
}

import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class CreditUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.credit.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  scoreInput: ElementFinder = element(by.css('input#credit-score'));

  nameInput: ElementFinder = element(by.css('input#credit-name'));

  timeInput: ElementFinder = element(by.css('input#credit-time'));

  currencyUnitInput: ElementFinder = element(by.css('input#credit-currencyUnit'));

  invoiceSelect = element(by.css('select#credit-invoice'));
}

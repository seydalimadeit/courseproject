/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import CreditComponentsPage, { CreditDeleteDialog } from './credit.page-object';
import CreditUpdatePage from './credit-update.page-object';
import CreditDetailsPage from './credit-details.page-object';

import {
  clear,
  click,
  getRecordsCount,
  isVisible,
  selectLastOption,
  waitUntilAllDisplayed,
  waitUntilAnyDisplayed,
  waitUntilCount,
  waitUntilDisplayed,
  waitUntilHidden,
} from '../../util/utils';

const expect = chai.expect;

describe('Credit e2e test', () => {
  let navBarPage: NavBarPage;
  let updatePage: CreditUpdatePage;
  let detailsPage: CreditDetailsPage;
  let listPage: CreditComponentsPage;
  let deleteDialog: CreditDeleteDialog;
  let beforeRecordsCount = 0;
  const username = process.env.E2E_USERNAME ?? 'admin';
  const password = process.env.E2E_PASSWORD ?? 'admin';

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    await navBarPage.login(username, password);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });

  it('should load Credits', async () => {
    await navBarPage.getEntityPage('credit');
    listPage = new CreditComponentsPage();

    await waitUntilAllDisplayed([listPage.title, listPage.footer]);

    expect(await listPage.title.getText()).not.to.be.empty;
    expect(await listPage.createButton.isEnabled()).to.be.true;

    await waitUntilAnyDisplayed([listPage.noRecords, listPage.table]);
    beforeRecordsCount = (await isVisible(listPage.noRecords)) ? 0 : await getRecordsCount(listPage.table);
  });
  describe('Create flow', () => {
    it('should load create Credit page', async () => {
      await listPage.createButton.click();
      updatePage = new CreditUpdatePage();

      await waitUntilAllDisplayed([updatePage.title, updatePage.footer, updatePage.saveButton]);

      expect(await updatePage.title.getAttribute('id')).to.match(/saveCashBankApp.credit.home.createOrEditLabel/);
    });

    it('should create and save Credits', async () => {
      await updatePage.scoreInput.sendKeys('5');
      expect(await updatePage.scoreInput.getAttribute('value')).to.eq('5');

      await updatePage.nameInput.sendKeys('5');
      expect(await updatePage.nameInput.getAttribute('value')).to.eq('5');

      await updatePage.timeInput.sendKeys('time');
      expect(await updatePage.timeInput.getAttribute('value')).to.match(/time/);

      await updatePage.currencyUnitInput.sendKeys('currencyUnit');
      expect(await updatePage.currencyUnitInput.getAttribute('value')).to.match(/currencyUnit/);

      // await selectLastOption(updatePage.invoiceSelect);

      expect(await updatePage.saveButton.isEnabled()).to.be.true;
      await updatePage.saveButton.click();

      await waitUntilHidden(updatePage.saveButton);
      expect(await isVisible(updatePage.saveButton)).to.be.false;

      await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      expect(await listPage.records.count()).to.eq(beforeRecordsCount + 1);
    });

    describe('Details, Update, Delete flow', () => {
      after(async () => {
        const deleteButton = listPage.getDeleteButton(listPage.records.last());
        await click(deleteButton);

        deleteDialog = new CreditDeleteDialog();
        await waitUntilDisplayed(deleteDialog.dialog);

        expect(await deleteDialog.title.getAttribute('id')).to.match(/saveCashBankApp.credit.delete.question/);

        await click(deleteDialog.confirmButton);
        await waitUntilHidden(deleteDialog.dialog);

        expect(await isVisible(deleteDialog.dialog)).to.be.false;

        await waitUntilCount(listPage.records, beforeRecordsCount);
        expect(await listPage.records.count()).to.eq(beforeRecordsCount);
      });

      it('should load details Credit page and fetch data', async () => {
        const detailsButton = listPage.getDetailsButton(listPage.records.last());
        await click(detailsButton);

        detailsPage = new CreditDetailsPage();

        await waitUntilAllDisplayed([detailsPage.title, detailsPage.backButton, detailsPage.firstDetail]);

        expect(await detailsPage.title.getText()).not.to.be.empty;
        expect(await detailsPage.firstDetail.getText()).not.to.be.empty;

        await click(detailsPage.backButton);
        await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      });

      it('should load edit Credit page, fetch data and update', async () => {
        const editButton = listPage.getEditButton(listPage.records.last());
        await click(editButton);

        await waitUntilAllDisplayed([updatePage.title, updatePage.footer, updatePage.saveButton]);

        expect(await updatePage.title.getText()).not.to.be.empty;

        await clear(updatePage.scoreInput);
        await updatePage.scoreInput.sendKeys('6');
        expect(await updatePage.scoreInput.getAttribute('value')).to.eq('6');

        await clear(updatePage.nameInput);
        await updatePage.nameInput.sendKeys('6');
        expect(await updatePage.nameInput.getAttribute('value')).to.eq('6');

        await updatePage.timeInput.clear();
        await updatePage.timeInput.sendKeys('modified');
        expect(await updatePage.timeInput.getAttribute('value')).to.match(/modified/);

        await updatePage.currencyUnitInput.clear();
        await updatePage.currencyUnitInput.sendKeys('modified');
        expect(await updatePage.currencyUnitInput.getAttribute('value')).to.match(/modified/);

        await updatePage.saveButton.click();

        await waitUntilHidden(updatePage.saveButton);

        expect(await isVisible(updatePage.saveButton)).to.be.false;
        await waitUntilCount(listPage.records, beforeRecordsCount + 1);
      });
    });
  });
});

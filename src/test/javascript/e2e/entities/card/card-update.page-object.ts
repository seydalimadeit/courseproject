import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class CardUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('saveCashBankApp.card.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  scoreInput: ElementFinder = element(by.css('input#card-score'));

  numberInput: ElementFinder = element(by.css('input#card-number'));

  cvvInput: ElementFinder = element(by.css('input#card-cvv'));

  dateInput: ElementFinder = element(by.css('input#card-date'));

  invoiceSelect = element(by.css('select#card-invoice'));
}

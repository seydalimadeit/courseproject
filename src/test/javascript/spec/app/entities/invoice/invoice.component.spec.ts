/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import InvoiceComponent from '@/entities/invoice/invoice.vue';
import InvoiceClass from '@/entities/invoice/invoice.component';
import InvoiceService from '@/entities/invoice/invoice.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Invoice Management Component', () => {
    let wrapper: Wrapper<InvoiceClass>;
    let comp: InvoiceClass;
    let invoiceServiceStub: SinonStubbedInstance<InvoiceService>;

    beforeEach(() => {
      invoiceServiceStub = sinon.createStubInstance<InvoiceService>(InvoiceService);
      invoiceServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<InvoiceClass>(InvoiceComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          invoiceService: () => invoiceServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      invoiceServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllInvoices();
      await comp.$nextTick();

      // THEN
      expect(invoiceServiceStub.retrieve.called).toBeTruthy();
      expect(comp.invoices[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      invoiceServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeInvoice();
      await comp.$nextTick();

      // THEN
      expect(invoiceServiceStub.delete.called).toBeTruthy();
      expect(invoiceServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});

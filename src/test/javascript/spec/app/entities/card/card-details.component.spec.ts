/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import CardDetailComponent from '@/entities/card/card-details.vue';
import CardClass from '@/entities/card/card-details.component';
import CardService from '@/entities/card/card.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Card Management Detail Component', () => {
    let wrapper: Wrapper<CardClass>;
    let comp: CardClass;
    let cardServiceStub: SinonStubbedInstance<CardService>;

    beforeEach(() => {
      cardServiceStub = sinon.createStubInstance<CardService>(CardService);

      wrapper = shallowMount<CardClass>(CardDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { cardService: () => cardServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCard = { id: 123 };
        cardServiceStub.find.resolves(foundCard);

        // WHEN
        comp.retrieveCard(123);
        await comp.$nextTick();

        // THEN
        expect(comp.card).toBe(foundCard);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCard = { id: 123 };
        cardServiceStub.find.resolves(foundCard);

        // WHEN
        comp.beforeRouteEnter({ params: { cardId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.card).toBe(foundCard);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});

/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CardComponent from '@/entities/card/card.vue';
import CardClass from '@/entities/card/card.component';
import CardService from '@/entities/card/card.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Card Management Component', () => {
    let wrapper: Wrapper<CardClass>;
    let comp: CardClass;
    let cardServiceStub: SinonStubbedInstance<CardService>;

    beforeEach(() => {
      cardServiceStub = sinon.createStubInstance<CardService>(CardService);
      cardServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CardClass>(CardComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          cardService: () => cardServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      cardServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCards();
      await comp.$nextTick();

      // THEN
      expect(cardServiceStub.retrieve.called).toBeTruthy();
      expect(comp.cards[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      cardServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeCard();
      await comp.$nextTick();

      // THEN
      expect(cardServiceStub.delete.called).toBeTruthy();
      expect(cardServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});

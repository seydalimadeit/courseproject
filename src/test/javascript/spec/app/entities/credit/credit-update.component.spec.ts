/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import * as config from '@/shared/config/config';
import CreditUpdateComponent from '@/entities/credit/credit-update.vue';
import CreditClass from '@/entities/credit/credit-update.component';
import CreditService from '@/entities/credit/credit.service';

import InvoiceService from '@/entities/invoice/invoice.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Credit Management Update Component', () => {
    let wrapper: Wrapper<CreditClass>;
    let comp: CreditClass;
    let creditServiceStub: SinonStubbedInstance<CreditService>;

    beforeEach(() => {
      creditServiceStub = sinon.createStubInstance<CreditService>(CreditService);

      wrapper = shallowMount<CreditClass>(CreditUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          creditService: () => creditServiceStub,

          invoiceService: () => new InvoiceService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.credit = entity;
        creditServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(creditServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.credit = entity;
        creditServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(creditServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCredit = { id: 123 };
        creditServiceStub.find.resolves(foundCredit);
        creditServiceStub.retrieve.resolves([foundCredit]);

        // WHEN
        comp.beforeRouteEnter({ params: { creditId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.credit).toBe(foundCredit);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});

/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CreditComponent from '@/entities/credit/credit.vue';
import CreditClass from '@/entities/credit/credit.component';
import CreditService from '@/entities/credit/credit.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Credit Management Component', () => {
    let wrapper: Wrapper<CreditClass>;
    let comp: CreditClass;
    let creditServiceStub: SinonStubbedInstance<CreditService>;

    beforeEach(() => {
      creditServiceStub = sinon.createStubInstance<CreditService>(CreditService);
      creditServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CreditClass>(CreditComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          creditService: () => creditServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      creditServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllCredits();
      await comp.$nextTick();

      // THEN
      expect(creditServiceStub.retrieve.called).toBeTruthy();
      expect(comp.credits[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      creditServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeCredit();
      await comp.$nextTick();

      // THEN
      expect(creditServiceStub.delete.called).toBeTruthy();
      expect(creditServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});

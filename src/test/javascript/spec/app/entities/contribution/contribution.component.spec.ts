/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ContributionComponent from '@/entities/contribution/contribution.vue';
import ContributionClass from '@/entities/contribution/contribution.component';
import ContributionService from '@/entities/contribution/contribution.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Contribution Management Component', () => {
    let wrapper: Wrapper<ContributionClass>;
    let comp: ContributionClass;
    let contributionServiceStub: SinonStubbedInstance<ContributionService>;

    beforeEach(() => {
      contributionServiceStub = sinon.createStubInstance<ContributionService>(ContributionService);
      contributionServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ContributionClass>(ContributionComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          contributionService: () => contributionServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      contributionServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllContributions();
      await comp.$nextTick();

      // THEN
      expect(contributionServiceStub.retrieve.called).toBeTruthy();
      expect(comp.contributions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      contributionServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      comp.removeContribution();
      await comp.$nextTick();

      // THEN
      expect(contributionServiceStub.delete.called).toBeTruthy();
      expect(contributionServiceStub.retrieve.callCount).toEqual(1);
    });
  });
});

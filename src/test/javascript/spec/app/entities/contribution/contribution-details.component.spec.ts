/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ContributionDetailComponent from '@/entities/contribution/contribution-details.vue';
import ContributionClass from '@/entities/contribution/contribution-details.component';
import ContributionService from '@/entities/contribution/contribution.service';
import router from '@/router';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Contribution Management Detail Component', () => {
    let wrapper: Wrapper<ContributionClass>;
    let comp: ContributionClass;
    let contributionServiceStub: SinonStubbedInstance<ContributionService>;

    beforeEach(() => {
      contributionServiceStub = sinon.createStubInstance<ContributionService>(ContributionService);

      wrapper = shallowMount<ContributionClass>(ContributionDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { contributionService: () => contributionServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundContribution = { id: 123 };
        contributionServiceStub.find.resolves(foundContribution);

        // WHEN
        comp.retrieveContribution(123);
        await comp.$nextTick();

        // THEN
        expect(comp.contribution).toBe(foundContribution);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundContribution = { id: 123 };
        contributionServiceStub.find.resolves(foundContribution);

        // WHEN
        comp.beforeRouteEnter({ params: { contributionId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.contribution).toBe(foundContribution);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});

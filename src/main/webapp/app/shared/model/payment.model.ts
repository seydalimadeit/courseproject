import { IInvoice } from '@/shared/model/invoice.model';

export interface IPayment {
  id?: number;
  name?: string | null;
  bankAccountNumber?: number | null;
  scoreNumber?: number | null;
  invoice?: IInvoice | null;
}

export class Payment implements IPayment {
  constructor(
    public id?: number,
    public name?: string | null,
    public bankAccountNumber?: number | null,
    public scoreNumber?: number | null,
    public invoice?: IInvoice | null
  ) {}
}

import { IUser } from '@/shared/model/user.model';

export interface IInvoice {
  id?: number;
  score?: string | null;
  currencyUnit?: string | null;
  user?: IUser | null;
}

export class Invoice implements IInvoice {
  constructor(public id?: number, public score?: string | null, public currencyUnit?: string | null, public user?: IUser | null) {}
}

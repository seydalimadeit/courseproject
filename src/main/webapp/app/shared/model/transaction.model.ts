import { IInvoice } from '@/shared/model/invoice.model';

export interface ITransaction {
  id?: number;
  currencyUnit?: string | null;
  date?: string | null;
  invoice?: IInvoice | null;
}

export class Transaction implements ITransaction {
  constructor(public id?: number, public currencyUnit?: string | null, public date?: string | null, public invoice?: IInvoice | null) {}
}

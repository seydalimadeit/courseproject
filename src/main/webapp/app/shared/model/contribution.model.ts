import { IInvoice } from '@/shared/model/invoice.model';

export interface IContribution {
  id?: number;
  name?: string | null;
  score?: number | null;
  time?: string | null;
  currencyUnit?: string | null;
  invoice?: IInvoice | null;
}

export class Contribution implements IContribution {
  constructor(
    public id?: number,
    public name?: string | null,
    public score?: number | null,
    public time?: string | null,
    public currencyUnit?: string | null,
    public invoice?: IInvoice | null
  ) {}
}

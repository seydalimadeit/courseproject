import { IInvoice } from '@/shared/model/invoice.model';

export interface ICard {
  id?: number;
  score?: number | null;
  number?: number | null;
  cvv?: number | null;
  date?: string | null;
  invoice?: IInvoice | null;
}

export class Card implements ICard {
  constructor(
    public id?: number,
    public score?: number | null,
    public number?: number | null,
    public cvv?: number | null,
    public date?: string | null,
    public invoice?: IInvoice | null
  ) {}
}

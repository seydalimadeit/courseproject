import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Invoice = () => import('@/entities/invoice/invoice.vue');
// prettier-ignore
const InvoiceUpdate = () => import('@/entities/invoice/invoice-update.vue');
// prettier-ignore
const InvoiceDetails = () => import('@/entities/invoice/invoice-details.vue');
// prettier-ignore
const Transaction = () => import('@/entities/transaction/transaction.vue');
// prettier-ignore
const TransactionUpdate = () => import('@/entities/transaction/transaction-update.vue');
// prettier-ignore
const TransactionDetails = () => import('@/entities/transaction/transaction-details.vue');
// prettier-ignore
const Card = () => import('@/entities/card/card.vue');
// prettier-ignore
const CardUpdate = () => import('@/entities/card/card-update.vue');
// prettier-ignore
const CardDetails = () => import('@/entities/card/card-details.vue');
// prettier-ignore
const Payment = () => import('@/entities/payment/payment.vue');
// prettier-ignore
const PaymentUpdate = () => import('@/entities/payment/payment-update.vue');
// prettier-ignore
const PaymentDetails = () => import('@/entities/payment/payment-details.vue');
// prettier-ignore
const Contribution = () => import('@/entities/contribution/contribution.vue');
// prettier-ignore
const ContributionUpdate = () => import('@/entities/contribution/contribution-update.vue');
// prettier-ignore
const ContributionDetails = () => import('@/entities/contribution/contribution-details.vue');
// prettier-ignore
const Credit = () => import('@/entities/credit/credit.vue');
// prettier-ignore
const CreditUpdate = () => import('@/entities/credit/credit-update.vue');
// prettier-ignore
const CreditDetails = () => import('@/entities/credit/credit-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/invoice',
    name: 'Invoice',
    component: Invoice,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/invoice/new',
    name: 'InvoiceCreate',
    component: InvoiceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/invoice/:invoiceId/edit',
    name: 'InvoiceEdit',
    component: InvoiceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/invoice/:invoiceId/view',
    name: 'InvoiceView',
    component: InvoiceDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/transaction',
    name: 'Transaction',
    component: Transaction,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/transaction/new',
    name: 'TransactionCreate',
    component: TransactionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/transaction/:transactionId/edit',
    name: 'TransactionEdit',
    component: TransactionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/transaction/:transactionId/view',
    name: 'TransactionView',
    component: TransactionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/card',
    name: 'Card',
    component: Card,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/card/new',
    name: 'CardCreate',
    component: CardUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/card/:cardId/edit',
    name: 'CardEdit',
    component: CardUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/card/:cardId/view',
    name: 'CardView',
    component: CardDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/payment',
    name: 'Payment',
    component: Payment,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/payment/new',
    name: 'PaymentCreate',
    component: PaymentUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/payment/:paymentId/edit',
    name: 'PaymentEdit',
    component: PaymentUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/payment/:paymentId/view',
    name: 'PaymentView',
    component: PaymentDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/contribution',
    name: 'Contribution',
    component: Contribution,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/contribution/new',
    name: 'ContributionCreate',
    component: ContributionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/contribution/:contributionId/edit',
    name: 'ContributionEdit',
    component: ContributionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/contribution/:contributionId/view',
    name: 'ContributionView',
    component: ContributionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/credit',
    name: 'Credit',
    component: Credit,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/credit/new',
    name: 'CreditCreate',
    component: CreditUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/credit/:creditId/edit',
    name: 'CreditEdit',
    component: CreditUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/credit/:creditId/view',
    name: 'CreditView',
    component: CreditDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];

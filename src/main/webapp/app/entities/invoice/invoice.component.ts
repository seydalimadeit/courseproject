import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IInvoice } from '@/shared/model/invoice.model';

import InvoiceService from './invoice.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Invoice extends Vue {
  @Inject('invoiceService') private invoiceService: () => InvoiceService;
  private removeId: number = null;

  public invoices: IInvoice[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllInvoices();
  }

  public clear(): void {
    this.retrieveAllInvoices();
  }

  public retrieveAllInvoices(): void {
    this.isFetching = true;

    this.invoiceService()
      .retrieve()
      .then(
        res => {
          this.invoices = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IInvoice): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeInvoice(): void {
    this.invoiceService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('saveCashBankApp.invoice.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllInvoices();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}

import { Component, Vue, Inject } from 'vue-property-decorator';

import InvoiceService from '@/entities/invoice/invoice.service';
import { IInvoice } from '@/shared/model/invoice.model';

import { ICredit, Credit } from '@/shared/model/credit.model';
import CreditService from './credit.service';

const validations: any = {
  credit: {
    score: {},
    name: {},
    time: {},
    currencyUnit: {},
  },
};

@Component({
  validations,
})
export default class CreditUpdate extends Vue {
  @Inject('creditService') private creditService: () => CreditService;
  public credit: ICredit = new Credit();

  @Inject('invoiceService') private invoiceService: () => InvoiceService;

  public invoices: IInvoice[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.creditId) {
        vm.retrieveCredit(to.params.creditId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.credit.id) {
      this.creditService()
        .update(this.credit)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.credit.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.creditService()
        .create(this.credit)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.credit.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveCredit(creditId): void {
    this.creditService()
      .find(creditId)
      .then(res => {
        this.credit = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.invoiceService()
      .retrieve()
      .then(res => {
        this.invoices = res.data;
      });
  }
}

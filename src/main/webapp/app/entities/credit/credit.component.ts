import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICredit } from '@/shared/model/credit.model';

import CreditService from './credit.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Credit extends Vue {
  @Inject('creditService') private creditService: () => CreditService;
  private removeId: number = null;

  public credits: ICredit[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCredits();
  }

  public clear(): void {
    this.retrieveAllCredits();
  }

  public retrieveAllCredits(): void {
    this.isFetching = true;

    this.creditService()
      .retrieve()
      .then(
        res => {
          this.credits = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: ICredit): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCredit(): void {
    this.creditService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('saveCashBankApp.credit.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllCredits();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}

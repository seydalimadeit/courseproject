import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICredit } from '@/shared/model/credit.model';
import CreditService from './credit.service';

@Component
export default class CreditDetails extends Vue {
  @Inject('creditService') private creditService: () => CreditService;
  public credit: ICredit = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.creditId) {
        vm.retrieveCredit(to.params.creditId);
      }
    });
  }

  public retrieveCredit(creditId) {
    this.creditService()
      .find(creditId)
      .then(res => {
        this.credit = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}

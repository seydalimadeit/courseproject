import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICard } from '@/shared/model/card.model';

import CardService from './card.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Card extends Vue {
  @Inject('cardService') private cardService: () => CardService;
  private removeId: number = null;

  public cards: ICard[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCards();
  }

  public clear(): void {
    this.retrieveAllCards();
  }

  public retrieveAllCards(): void {
    this.isFetching = true;

    this.cardService()
      .retrieve()
      .then(
        res => {
          this.cards = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: ICard): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCard(): void {
    this.cardService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('saveCashBankApp.card.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllCards();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}

import { Component, Vue, Inject } from 'vue-property-decorator';

import InvoiceService from '@/entities/invoice/invoice.service';
import { IInvoice } from '@/shared/model/invoice.model';

import { ICard, Card } from '@/shared/model/card.model';
import CardService from './card.service';

const validations: any = {
  card: {
    score: {},
    number: {},
    cvv: {},
    date: {},
  },
};

@Component({
  validations,
})
export default class CardUpdate extends Vue {
  @Inject('cardService') private cardService: () => CardService;
  public card: ICard = new Card();

  @Inject('invoiceService') private invoiceService: () => InvoiceService;

  public invoices: IInvoice[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cardId) {
        vm.retrieveCard(to.params.cardId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.card.id) {
      this.cardService()
        .update(this.card)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.card.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    } else {
      this.cardService()
        .create(this.card)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('saveCashBankApp.card.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        });
    }
  }

  public retrieveCard(cardId): void {
    this.cardService()
      .find(cardId)
      .then(res => {
        this.card = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.invoiceService()
      .retrieve()
      .then(res => {
        this.invoices = res.data;
      });
  }
}
